﻿public class DelegateFunction
{
    public List<T> Where<T>(List<T> numbers, Predicate<T> predicate)
    {
        List<T> result = new List<T>();
        foreach (T t in numbers)
        {
            if (predicate(t))
            {
                result.Add(t);
            }
        }
        return result;
    }


}