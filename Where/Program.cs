﻿using Where;

List<int> numbers = new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
Console.Write("Initial list: ");
foreach (var number in numbers)
{
    Console.Write(number + " ");
}
Console.WriteLine();
DelegateFunction df = new DelegateFunction();
List<int> numbersDivisibleBy3 = df.Where(numbers, x => x % 3 == 0);
Console.Write("List number are divisible by 3: ");
foreach (var number in numbersDivisibleBy3)
{
    Console.Write(number + " ");
}

List<string> strings = new List<string> { "ahihi", "hihihih", "aaa" };
Console.Write("\nInitial list: ");
foreach (var item in strings)
{
    Console.Write(item + " ");
}
Console.WriteLine();
List<string> strings1 = df.Where(strings, x => x.Length >= 5);
Console.Write("List string have lengh >= 5: ");
foreach (var item in strings1)
{
    Console.Write(item + " ");
}